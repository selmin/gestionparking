﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace miniprojet.Models
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }

        public string idClient { get; set; }

        public int idParking { get; set; }

        public string dateDebut { get; set; }

        public string dateFin { get; set; }

        /*[ForeignKey("idClient")]
        public AspNetUser AspNetUser { get; set; }*/

        [ForeignKey("idParking")]
        public Parking? Parking { get; set; }
    }
}

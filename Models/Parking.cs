﻿using System.ComponentModel.DataAnnotations;

namespace miniprojet.Models
{
    public class Parking
    {
        [Key]
        public int Id { get; set; }

        public string? nom { get; set; }

        public string? adresse { get; set; }

        public int nbrPlaceLibre { get; set; }

        public int nbrPlaceOccupe { get; set; }

    }
}
